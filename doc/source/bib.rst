References
==========

Bogdan Dumitrescu and Paul Irofti.
Dictionary learning algorithms and applications. Springer, 2018.

Q. Zhang, B. Li, Discriminative K-SVD for dictionary learning in face recognition, in
Proceedings of IEEE Conf. Computer Vision and Pattern Recognition (2010), pp. 2691–2698

Z. Jiang, Z. Lin, L.S. Davis, Label consistent K-SVD: learning a discriminative dictionary
for recognition. IEEE Trans. Pattern Anal. Mach. Intell. 35(11), 2651–2664 (2013)

Gu, Shuhang, et al. "Projective dictionary pair learning for pattern classification."
Advances in neural information processing systems 27 (2014): 793-801