===========
Classifiers
===========

Discriminative Dictionary Learning
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. autoclass:: dictlearn.models.DDL
    :members:

Kernel Discriminative Dictionary Learning
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. autoclass:: dictlearn.models.KDDL
    :members:

Label Consistent Dictionary Learning
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. autoclass:: dictlearn.models.LCDL
    :members:

Kernel Label Consistent Dictionary Learning
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. autoclass:: dictlearn.models.KLCDL
    :members:

Dictionary Pair Learning
~~~~~~~~~~~~~~~~~~~~~~~~
.. autoclass:: dictlearn.models.DPL
    :members:

Kernel Dictionary Pair Learning
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. autoclass:: dictlearn.models.KDPL
    :members: