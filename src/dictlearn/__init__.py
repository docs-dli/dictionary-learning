__version__ = '0.0.1'

from ._dictionary_learning import (
    sparse_encode,
    dictionary_learning,
    kernel_dictionary_learning,
    online_dictionary_learning,
    DictionaryLearning,
    _get_fit_handle
)
